float bruteForce(struct point P[], int n,float dmin,struct point min[2])
{
	float dis = 9999;

	for (int i = 0; i < n; ++i)
	{
		for (int j = i+1; j < n; ++j)
		{
			dis=dist(P[i],P[j]);
			if(dis<dmin)
			{
			    min[0]=P[i];
			    min[1]=P[j];
			    dmin=dis;
			}
	    }
	}

	return dmin;
}



