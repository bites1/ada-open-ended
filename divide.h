float findpairs( struct point p[],int l,int h,struct point min[],int n,float dmin)
{
    int mid;
    float left,right;

    
    if(n==1)
    {
        dmin=dist(p[0],p[0]);
        min[0]=p[0];
        min[1]=p[0];
        return dmin;
    }

    if(l+1==h || l+2==h)
    {
        dmin=bruteForce(p,n,dmin,min);
    }
    else
    {
         mid=(l+h)/2;
        left=findpairs(p,l,mid,min,mid,dmin);
        right=findpairs(p,mid+1,h,min,n-mid,dmin);
        dmin=left<right?left:right;
       
    }
    return dmin;
