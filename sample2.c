#include<stdio.h>
#include<stdlib.h>
#include"sample0.h"
#include"sample.h"
#include"sample1.h"
#include"divide.h"




int main()
{
    int i,n,low,high;
    struct point p[100],min[2];
    float dmin=9999;


    printf("\nEnter the number of points:");
    scanf("%d",&n);
    
    low=0;
    high=n-1;

    for(i=0;i<n;i++)
    {
        printf("Enter the point%d: ",i+1);
        scanf("%d%d",&p[i].x,&p[i].y);
    }

      dmin=findpairs(p,low,high,min,n,dmin);

    printf("\nThe closest pair of points among the given is (%d,%d) and (%d,%d)\n",min[0].x,min[0].y,min[1].x,min[1].y);
    printf("\nThe minimum distance is %f\n",dmin);

    return 0;
}

