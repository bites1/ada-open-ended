ALGORITHM:bruteForce(p,n,dmin,min)
//To find the closest pairs among the set of planes by brute force
//Input:Set of pair of points
//Output:shortest distance and the corresponding pair of points
   dmin<-INF
   for i<-1 to n-1 do
       for j<- i+1 to n do
            d<-(sqrt((diff of x coordinate)*(diff of x coordinate)+(diff of y coordinate)*(diff of y coordinate)))
            if d<dmin
                dmin<-d
                min[0]<-p[i]
                min[1]<-p[j]

   return dmin 



ALGORITHM:findpairs(p,l,h,min,n,dmin)
//Find the closest pair of points based on divide and conquer technique
//Input:Set of pair of points
//Output:shortest distance and the corresponding pair of points is stored in structure array
 
    if n=1 //The given pair is closest pair of itself
    
        dmin<-(sqrt((diff of x coordinate)*(diff of x coordinate)+(diff of y coordinate)*(diff of y coordinate)))
        min[0]<-p[0]
        min[1]<-p[0]
    return dmin
    

    if(l+1==h || l+2==h)
    
        dmin<-bruteForce(p,n,dmin,min)
    
    else
    
        mid<-(l+h)/2;
        left<-findpairs(p,l,mid,min,n,dmin)
        right<-findpairs(p,mid+1,h,min,n,dmin)
       
    if left<right
         dmin<-left
    else 
          dmin<-right

    
    return dmin

